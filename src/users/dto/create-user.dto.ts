import { IsNotEmpty, Length, Matches } from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @Length(3, 16)
  login: string;

  @Length(3, 16)
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  password: string;
}
